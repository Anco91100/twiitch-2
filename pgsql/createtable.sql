create table viewers
(
    id SERIAL PRIMARY KEY,
    username varchar(255),
    nbmessages integer
);