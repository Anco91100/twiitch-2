'use strict';

function minuteToMilliseconds(x)
{
    return x * 1000;
}

module.exports = { minuteToMilliseconds };