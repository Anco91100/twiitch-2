"use strict";

const utilities = require('./utilities.js');

const config = require('./config.json');

const TwitchAPI = require('./twitch/twitch_api.js');
const express = require('express');
const path = require('path');
const app = express();
const port = 8081;

let StatsManager = require('./stats_engine/stats_manager.js');

let Database = require('./database/database.js');
Database.connect();

let s = new StatsManager(config.twitch.username, config.twitch['oauth-password'], false, config);




function start()
{
	app.use(express.static(__dirname + "/front/"));
	(function _start()
	{
		TwitchAPI.getStreamList(100, (err, res) =>
		{
			if(err) 
			{
				console.log(err );
				return;
			}
			if(!res.data) 
			{
				console.log('no streams');
				return;
			}
			for(let stream of res.data)
				s.addChannel(stream);

			console.log('New TOP stream list.');
		});
		
		TwitchAPI.getTopGames(20, (err, res) =>
		{
			
			if(err) 
			{
				console.log(err);
				return;	
			};

			if(!res.data || res.data.length === 0)
			{
				return;
			} 

			for(let game of res.data)
			{
				TwitchAPI.getTopStreamsByGame(encodeURI(game.name), 20, (err, res) =>
				{
					if(err){
						console.log(err);
						return;
					}

					if(!res.data)
					{
						console.log('no streams in game from games');
					} 

					for(let stream of res.data)
						s.addChannel(stream);
				});
			}

			console.log('Channels got from Twitch')
		});

		setTimeout(_start, utilities.minuteToMilliseconds(5));
	})();
}

start();

