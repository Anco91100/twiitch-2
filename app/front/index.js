
/*<h4><small>RECENT LIVE</small></h4>
      <hr>
      <h2>I Love Food</h2>
      <h5><span class="glyphicon glyphicon-time"></span> Post by Jane Dane, Sep 27, 2015.</h5>
      <h5><span class="label label-danger">Food</span> <span class="label label-primary">Ipsum</span></h5><br>
      <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <br><br>
      <hr>   */
const game = {
    "games":[    
        {
            "id": "1",
            "name": "Fortnite",
            "description": "Fortnite est un jeu en ligne développé par Epic Games sous la forme de différents modes de jeu qui partagent le même gameplay général et le même moteur de jeu."
        },
        {
            "id": "2",
            "name": "Among US",
            "description": "Among Us est un jeu vidéo d'ambiance multijoueur en ligne développé et édité par le studio InnerSloth, sorti en 2018 sur Android, iOS, Chrome OS puis Windows, en 2020 sur Nintendo Switch et en 2021 sur PlayStation 4 et PlayStation 5. Le jeu se déroule dans un univers de science-fiction."
        },
        {
            "id": "3",
            "name": "Call of Duty: Warzone",
            "description": "Call of Duty: Warzone est un jeu vidéo de battle royale développé par Infinity Ward et Raven Software et édité par Activision, sorti le 10 mars 2020 sur Xbox One, PlayStation 4 et Microsoft Windows."
        },
    ]
};

const makeList = (list)=>{
    var titleLive = document.createElement('h4');
    var hr = document.createElement('hr');
    var br = document.createElement('br');
    var title = document.createElement('h2');
    var description = document.createElement('p');
    var content = "";

    array = list.games;

    for (var i = 0; i < array.length; i++) {
        var obj = array[i];
        content = content +"<h4><small> RECENT LIVE &#128308; </small></h4>"+ "<h2>"+ obj.name +" &#127918;</h2>"+"<p>"+obj.description+"</p><br><br><hr>"
        console.log(content);
    }
return content;
};
document.getElementById('loopList').innerHTML = makeList(game)