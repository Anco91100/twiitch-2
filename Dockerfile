FROM node:12.18.1
WORKDIR /app
COPY ["package.json", "./"]
RUN npm install
RUN npm install pg
COPY ./app/ .
CMD ["npm", "run","start", ">", "my_app_log.log", " 2>", "my_app_err.log"]